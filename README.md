# Deal-widget
This is a project just for practice in HTML and CSS
The design is from [Dribbble](https://dribbble.com/shots/5925003-Kizen-App-Deal-Widget?fbclid=IwAR3nlsoc8T2i8_BEI5QDlMngNvuCXU8QLqyd-MdP8olyf4W4JCGdYgB3T6E)

## TODO
- [ ] use grid for quick-view section for better layout
- [ ] corrections to margins-paddings
- [ ] corrections to font-sizes
- [ ] use ellipsis for text-overflow
- [ ] replace the progress bar with the correct one using divs
